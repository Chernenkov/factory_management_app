package com.cornel.pims.core

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

class Formatter {
    companion object {
        fun format(x: Double): String {
            val df = DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH))
            df.maximumFractionDigits = 340 //340 = DecimalFormat.DOUBLE_FRACTION_DIGITS
            val result = df.format(x)
            return if (result.endsWith(".0")) result.substring(0, result.length - 2) else result
        }
    }
}