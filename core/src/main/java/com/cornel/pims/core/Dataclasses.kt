package com.cornel.pims.core

data class OAuthJsonResponse(
        val access_token: String,
        val token_type: String,
        val expire_in: String,
        val refresh_token: String,
        val scope: String,
        val created_at: String)

enum class BarcodeType(val value: String) {
    SKU("sku"),
    AREA("wa"),
    STORAGE("wh"),
    WORKPLACE("wp"),
    WORKER("wo"),
    WORKSHOP("fa"),
    ASSIGNMENT("po");
}

data class BarcodeRaw(
        val v: Int,
        val t: String,
        val id: String
)

data class Barcode(
        val version: Int,
        val type: BarcodeType,
        val id: String
)