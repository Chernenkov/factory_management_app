package com.cornel.pims.qc.auth

import dagger.Module
import dagger.Provides

/**
 * Created by AlexGator on 28.02.18.
 */

@Module
class QCAuthModule(private val activity: QCAuthView) {

    @QCAuthScope
    @Provides
    fun providePresenter(): QCAuthPresenter
            = QCAuthPresenterImpl(activity)

    @QCAuthScope
    @Provides
    fun provideActivity(): QCAuthView = activity
}
