package com.cornel.pims.qc.productinfo

import dagger.Module
import dagger.Provides

/**
 * Created by AlexGator on 13.03.18.
 */

@Module
class QCProductInfoModule(private val activity: QCProductInfoView) {
    @QCProductInfoScope
    @Provides
    fun providePresenter(): QCProductInfoPresenter = QCProductInfoPresenterImpl(activity)
}