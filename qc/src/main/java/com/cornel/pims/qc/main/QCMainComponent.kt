package com.cornel.pims.qc.main

import dagger.Subcomponent
import javax.inject.Scope

/**
 * Created by AlexGator on 13.03.18.
 */
@Scope annotation class QCMainScope

@QCMainScope
@Subcomponent(modules = [QCMainModule::class])
interface QCMainComponent {
    fun inject(activity: QCMainActivity)
    fun inject(presenter: QCMainPresenterImpl)
}