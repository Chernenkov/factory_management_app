package com.cornel.pims.qc.productinfo

import dagger.Subcomponent
import javax.inject.Scope

/**
 * Created by AlexGator on 13.03.18.
 */
@Scope annotation class QCProductInfoScope

@QCProductInfoScope
@Subcomponent(modules = [QCProductInfoModule::class])
interface QCProductInfoComponent {
    fun inject(activity: QCProductInfoActivity)
    fun inject(presenter: QCProductInfoPresenterImpl)
}