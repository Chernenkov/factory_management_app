package com.cornel.pims.qc.core

import java.text.SimpleDateFormat
import java.util.*

object Formats {
    val dateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale("ru"))
}

enum class QError {
        AUTH,
        TOKEN,
        SERVER
}

data class QResponse <T> (
        val data: T?,
        val error: QError?
)

data class User(
        val id: String,
        val name: String
)

data class Product(
        val id: String,
        val name: String,
        val code: String,
        val abbreviation: String
)

data class Workshop(
        val id: String,
        val code: String,
        val name: String
)

data class Workplace(
        val id: String,
        val code: String,
        val name: String,
        val producedQuantity: Int
)

enum class AssignmentState(val value: String) {
        ARRIVED("arrived_at_stage"),
        IN_PROGRESS("work_in_progress"),
        PAUSED("paused_work"),
        WAIT_QC("waiting_for_technical_control"),
        QC_IN_PROGRESS("technical_control_in_progress"),
        REJECTED("rejected"),
        WAIT_REV("waiting_for_revision"),
        REV_IN_PROGRESS("revision_in_progress"),
        WAIT_ACCEPT("waiting_for_acceptance"),
        ACCEPT_STORAGE("accepted_to_storage"),
        UNKNOWN("")
}

data class QCFactoryOperation(
        val id: String,
        val productionAssignments: List<QCProductionAssignment>)

data class QCProductionAssignment(
        val id: String,
        val code: String,
        val state: String,
        val deletedAt: String?,
        val quantity: Double,
        val productId: String,
        val operationName: String,
        val lastWorkplaceName: String,
        val workers: List<String>,
        val manager: String,
        val additionalInfo: String,
        val comment: String,
        val abbreviation: String
)
