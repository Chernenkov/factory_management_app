package com.cornel.pims.factory.workers.info

import dagger.Subcomponent
import javax.inject.Scope


/**
 * Created by AlexGator on 07.03.18.
 */

@Scope
annotation class FactoryWorkerInfoScope

@FactoryWorkerInfoScope
@Subcomponent(modules = [FactoryWorkerInfoModule::class])
interface FactoryWorkerInfoComponent {
    fun inject(activity: FactoryWorkerInfoActivity)
    fun inject(presenter: FactoryWorkerInfoPresenterImpl)
}

