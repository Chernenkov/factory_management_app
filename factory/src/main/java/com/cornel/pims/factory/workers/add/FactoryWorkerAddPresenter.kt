package com.cornel.pims.factory.workers.add

import android.content.Context
import com.cornel.pims.factory.R
import com.cornel.pims.factory.common.WorkerListUnit
import com.cornel.pims.factory.core.DataManager
import com.cornel.pims.factory.core.proto.BasePresenter
import com.cornel.pims.factory.core.proto.BasePresenterImpl
import com.cornel.pims.factory.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import javax.inject.Inject


/**
 * Created by AlexGator on 08.03.18.
 */
interface FactoryWorkerAddPresenter : BasePresenter {
    fun onCreate(id: String)
    fun onRefresh()
    fun onItemSelected(item: WorkerListUnit)
    fun onBackButtonPressed()
    fun onWorkerCodeScanned(code: String)
}

class FactoryWorkerAddPresenterImpl(
        private val view: FactoryWorkerAddView)
    : BasePresenterImpl(view as BaseView), FactoryWorkerAddPresenter {

    @Inject
    lateinit var dataManager: DataManager

    private lateinit var workplaceId: String

    private lateinit var item: WorkerListUnit

//    @Inject
//    lateinit var appContext: Context

    override fun onCreate(id: String) {
        view.component.inject(this)
        workplaceId = id
    }

    override fun doOnResumeAsync() {
        updateDate()
    }

    override fun onRefresh() {
        smartLaunch {
            updateDate()
        }
    }

    private fun updateDate() {
        runBlocking {
            val workers = get(dataManager.getUnassignedWorkerListByWorkplaceId(workplaceId))!!
                    .map {
                        WorkerListUnit(it.id, it.fullName)
                    }

            launch(UI) {
                view.showItems(workers)
            }
        }
    }

    override fun onItemSelected(item: WorkerListUnit) {

        this.item = item

        smartLaunch {
            runBlocking {

                val area = get(dataManager.getWorkplaceById(workplaceId))!!

//                val text = appContext.getString(R.string.factory_workers_register_confirm, item.name, area.name)
                val text = "Вы уверены, что хотите добавить работника ${item.name} на рабочую зону ${area.name}?"

//                val btnText = appContext.getString(R.string.factory_workers_register_button)
                val btnText = "Зарегистрировать"

                launch(UI) {
                    (view as BaseView).showConfirmation(text,
                            btnText,
                            {registerWorker()})

//                    doOnResumeAsync()
                }

//                view.finish()
            }
        }
    }

    override fun onWorkerCodeScanned(code: String) {
        launch(UI) {
            if (get(dataManager.getUnassignedWorkerListByWorkplaceId(workplaceId))!!.find { it.id == code } == null) {
                (view as BaseView).showError(RuntimeException("Не получилось зарегистрировать работника. Работник с таким идентификатором не найден в этом рабочем месте. Обратитесь к начальнику производства."), false)
            } else {
                val counts = get(dataManager.getWorkerCountsByWorkplaceId(workplaceId))!!
                if (counts.max == counts.current) {
                    (view as BaseView).showError(RuntimeException("Не удалось зарегистрировать сотрудника. Зарегистрировано максимально возможное количество сотрудников. Обратитесь к начальнику производства."), false)
                } else {
                    smartLaunch(onException = {
                        (view as BaseView).isOk(false)
                        super.onException(it)
                    }, finally = {
                        super.finally()
                        view.finish()
                    }
                    ) {
                        runBlocking {
                            dataManager.registerWorker(code, workplaceId)
                        }
                    }
                }
            }
        }
    }

    private fun registerWorker() {
        runBlocking {
            val counts = get(dataManager.getWorkerCountsByWorkplaceId(workplaceId))!!
            if (counts.max == counts.current) {
                (view as BaseView).showError(RuntimeException("Не удалось зарегистрировать сотрудника. Зарегистрировано максимально возможное количество сотрудников. Обратитесь к начальнику производства."), false)
            } else {
                smartLaunch(onException = {
                    (view as BaseView).isOk(false)
                    super.onException(it)
                }, finally = {
                    super.finally()
                    view.finish()
                }
                ) {
                    runBlocking {
                        dataManager.registerWorker(item.id, workplaceId)
                    }
                }
            }
        }
    }

    override fun onBackButtonPressed() {
        doOnResumeAsync()
        view.finish()
    }
}