package com.cornel.pims.factory.status.productionlist

import dagger.Subcomponent
import javax.inject.Scope


/**
 * Created by AlexGator on 07.03.18.
 */

@Scope
annotation class FactoryProductionListScope

@FactoryProductionListScope
@Subcomponent(modules = [FactoryProductionListModule::class])
interface FactoryProductionListComponent {
    fun inject(activity: FactoryProductionListActivity)
    fun inject(presenter: FactoryProductionListPresenterImpl)
}

