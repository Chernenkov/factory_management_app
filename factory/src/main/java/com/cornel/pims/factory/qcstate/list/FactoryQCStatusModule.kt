package com.cornel.pims.factory.qcstate.list

import dagger.Module
import dagger.Provides


/**
 * Created by AlexGator on 07.03.18.
 */


@Module
class FactoryQCStatusModule(private val activity: FactoryQCStatusView) {

    @FactoryQCStatusActivityScope
    @Provides
    fun providePresenter(): FactoryQCStatusPresenter = FactoryQCStatusPresenterImpl(activity)

    @FactoryQCStatusActivityScope
    @Provides
    fun provideActivity(): FactoryQCStatusView = activity
}
