package com.cornel.pims.factory.workers.add

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.cornel.pims.core.Barcode
import com.cornel.pims.core.BarcodeType
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.factory.R
import com.cornel.pims.factory.common.WorkerListUnit
import com.cornel.pims.factory.core.app.app
import com.cornel.pims.factory.core.proto.BaseView
import kotlinx.android.synthetic.main.common_list_with_go_back_button.*
import kotlinx.android.synthetic.main.common_worker_list_item.view.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject


/**
 * Created by AlexGator on 08.03.18.
 */

interface FactoryWorkerAddView {
    val component: FactoryWorkerAddComponent

    fun showItems(data: List<WorkerListUnit>)
    fun showError(e: Throwable)
    fun finish()
}

class FactoryWorkerAddActivity: BaseView(), FactoryWorkerAddView {

    companion object {
        const val EXTRA_WORKPLACE_ID = "workplace_id"
    }

    @Inject
    override lateinit var presenter: FactoryWorkerAddPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override val component by lazy {
        app.component.plus(FactoryWorkerAddModule(this))
    }

    override fun onBarcodeScan(barcode: Barcode) {
        if (barcode.type == BarcodeType.WORKER)
        presenter.onWorkerCodeScanned(barcode.id)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.common_list_with_go_back_button)
        setSupportActionBar(toolbar)

        component.inject(this)

        val id = intent.getStringExtra(EXTRA_WORKPLACE_ID)

        presenter.onCreate(id)

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        addListeners()
    }

    private fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        goBackBtn.onClick {
            presenter.onBackButtonPressed()
        }
    }

    override fun showItems(data: List<WorkerListUnit>) {
        recycler.swapAdapter(
                RecyclerAdapter(data, this::onRecyclerItemClick),
                false)
    }

    private fun onRecyclerItemClick(item: WorkerListUnit) = presenter.onItemSelected(item)

    class RecyclerAdapter(private val data: List<WorkerListUnit>,
                          private val listener: (WorkerListUnit) -> Unit)
        : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

        init { setHasStableIds(true) }

        class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
            var view               = v
            val itemName: TextView = v.itemName

            fun bind(item: WorkerListUnit, listener: (WorkerListUnit) -> Unit) {
                itemName.text   = item.name
                view.setOnClickListener { listener(item) }
            }
        }

        override fun getItemId(position: Int): Long = data[position].id.hashCode().toLong()

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = data[position]
            holder.bind(item, listener)
        }

        override fun getItemCount() = data.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.common_worker_list_item, parent, false)
            return ViewHolder(v)
        }
    }
}