package com.cornel.pims.factory.common

data class WorkshopListUnit(val id: String, val name: String)

data class WorkplaceListUnit(val id: String, val name: String, val count: Long)

data class WorkerListUnit(val id: String, val name: String)

data class WorkersWorkplaceListUnit(
        val id: String,
        val name: String, val
        productionQuantity: Int
)

data class QCListUnit(
        val id: String,
        val code: String,
        val name: String,
        val state: String,
        val shippingDate: String,
        val highlight: Boolean
)

data class ItemListUnit(
        val id: String,
        val name: String,
        val code: String,
        val quantity: Double)

data class HistoryUnit(
        val name: String,
        val date: String
)

data class ProductAssignmentListUnit(
        val id: String,
        val code: String,
        val name: String,
        val sku: String,
        val operation: String,
        val stateResId: Int,
        val colorResId: Int,
        val shipmentDate: String
)

data class WorkplaceUnit(
        val id: String,
        val name: String
)
