package com.cornel.pims.factory.status.productionlist

import android.content.Context
import com.cornel.pims.core.Formatter
import com.cornel.pims.factory.R
import com.cornel.pims.factory.common.ProductAssignmentListUnit
import com.cornel.pims.factory.core.AssignmentState
import com.cornel.pims.factory.core.DataManager
import com.cornel.pims.factory.core.proto.BasePresenter
import com.cornel.pims.factory.core.proto.BasePresenterImpl
import com.cornel.pims.factory.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import javax.inject.Inject


/**
 * Created by AlexGator on 08.03.18.
 */
interface FactoryProductionListPresenter : BasePresenter {
    fun onCreate(id: String)
    fun onRefresh()
    fun onItemSelected(item: ProductAssignmentListUnit)
    fun onBackButtonPressed()
}

class FactoryProductionListPresenterImpl(
        private val view: FactoryProductionListView)
    : BasePresenterImpl(view as BaseView), FactoryProductionListPresenter {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    private lateinit var workpaceId: String

    override fun onCreate(id: String) {
        view.component.inject(this)
        workpaceId = id
    }

    override fun doOnResumeAsync() {
        updateData()
    }

    override fun onRefresh() {
        smartLaunch {
            updateData()
        }
    }

    private fun updateData() {
        runBlocking {
            val workpace = get(dataManager.getWorkplaceById(workpaceId))!!
            val assignmentsDef = get(dataManager.getProductionAssignmentsByWorkplaceId(workpaceId))!!

            val assignments = mutableListOf<ProductAssignmentListUnit>()
            assignmentsDef.map {
                if (stateMap.containsKey(it.state)) {
                    val quantity = appContext.getString(
                            R.string.factory_status_production_list_quantity,
                            Formatter.format(it.quantity))

                    val product = get(dataManager.getProductById(it.productId))!!
                    assignments.add(ProductAssignmentListUnit(
                            it.id,
                            it.code,
                            product.name,
                            it.productSku,
                            it.currentOperation?.name ?: "",
                            stateMap[it.state] ?: stateMap[AssignmentState.UNKNOWN]!!,
                            colorMap[it.state] ?: colorMap[AssignmentState.UNKNOWN]!!,
//                                TODO Real shippingDate
                            ""
                    ))
                }
            }
            launch(UI) {
                view.showItems(assignments)
                view.setTitle(workpace.name)
            }
        }
    }

    override fun onItemSelected(item: ProductAssignmentListUnit) = view.navigateToProductInfo(item.id, workpaceId)

    override fun onBackButtonPressed() = view.finish()


    companion object {
        val colorMap = mapOf(
                AssignmentState.ARRIVED to R.color.state_red,
                AssignmentState.IN_PROGRESS to R.color.state_green,
                AssignmentState.PAUSED to R.color.state_yellow,
                AssignmentState.WAIT_REV to R.color.state_red,
                AssignmentState.REV_IN_PROGRESS to R.color.state_green,
                AssignmentState.UNKNOWN to R.color.state_gray
        )
        val stateMap = mapOf(
                AssignmentState.ARRIVED to R.string.factory_status_production_list_state_arrived,
                AssignmentState.IN_PROGRESS to R.string.factory_status_production_list_state_in_progress,
                AssignmentState.PAUSED to R.string.factory_status_production_list_state_paused,
                AssignmentState.WAIT_REV to R.string.factory_status_production_list_state_wait_revision,
                AssignmentState.REV_IN_PROGRESS to R.string.factory_status_production_list_state_revision_in_progress,
                AssignmentState.UNKNOWN to R.string.factory_status_production_list_state_unknown

        )
    }
}