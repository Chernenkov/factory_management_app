package com.cornel.pims.factory.workers.add

import dagger.Subcomponent
import javax.inject.Scope


/**
 * Created by AlexGator on 07.03.18.
 */

@Scope
annotation class FactoryWorkerAddScope

@FactoryWorkerAddScope
@Subcomponent(modules = [FactoryWorkerAddModule::class])
interface FactoryWorkerAddComponent {
    fun inject(activity: FactoryWorkerAddActivity)
    fun inject(presenter: FactoryWorkerAddPresenterImpl)
}

