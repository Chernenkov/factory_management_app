package com.cornel.pims.factory.workers.workplace

import com.cornel.pims.factory.common.WorkplaceListUnit
import com.cornel.pims.factory.core.DataManager
import com.cornel.pims.factory.core.proto.BasePresenter
import com.cornel.pims.factory.core.proto.BasePresenterImpl
import com.cornel.pims.factory.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import javax.inject.Inject


/**
 * Created by AlexGator on 08.03.18.
 */
interface FactoryWorkersWorkplaceListPresenter : BasePresenter {
    fun onCreate()
    fun onRefresh()
    fun onItemSelected(item: WorkplaceListUnit)
    fun onBackButtonPressed()
}

class FactoryWorkersWorkplaceListPresenterImpl(
        private val view: FactoryWorkersWorkplaceListView)
    : BasePresenterImpl(view as BaseView), FactoryWorkersWorkplaceListPresenter {

    @Inject
    lateinit var dataManager: DataManager

    override fun onCreate() {
        view.component.inject(this)
    }

    override fun doOnResumeAsync() {
        updateData()
    }

    override fun onRefresh() {
        smartLaunch {
            updateData()
        }
    }

    private fun updateData() {
        runBlocking {
            val workshopId = dataManager.getCurrentWorkshopId()
            val workplaces = get(dataManager.getWorkplacesByWorkshopId(workshopId))!!.map {
                WorkplaceListUnit(it.id, it.name, it.assignedWorkersQuantity.toLong())
                //real workers count didn't take workersIds list size to reduce empty list risk
            }

            launch(UI) {
                view.apply {
                    showItems(workplaces)
                }
            }
        }
    }

    override fun onItemSelected(item: WorkplaceListUnit) {
        view.navigateToWorkerList(item.id, item.name)
    }

    override fun onBackButtonPressed() = view.finish()

}