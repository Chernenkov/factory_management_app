package com.cornel.pims.factory.main

import android.content.Context
import com.cornel.pims.factory.R
import com.cornel.pims.factory.common.WorkshopListUnit
import com.cornel.pims.factory.core.DataManager
import com.cornel.pims.factory.core.proto.BasePresenter
import com.cornel.pims.factory.core.proto.BasePresenterImpl
import com.cornel.pims.factory.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


/**
 * Created by AlexGator on 07.03.18.
 */

interface FactoryMainPresenter : BasePresenter {
    fun onCreate(workshopId: String?)
    fun onWorkshopChange(selected: WorkshopListUnit)
    fun onSettingsClick()
    fun onLogoutClick()
    fun onActionBtnClick(button: FactoryMainView.Button)
}

class FactoryMainPresenterImpl(private val view: FactoryMainView)
    : BasePresenterImpl(view as BaseView), FactoryMainPresenter {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    private var workshopId: String? = null


    override fun onCreate(workshopId: String?) {
        view.component.inject(this)
        this.workshopId = workshopId
    }

    override fun doOnResumeAsync() {
        runBlocking {
            val date = SimpleDateFormat("d MMM", Locale("ru")).format(Date())

            val user = get(dataManager.getUser())!!

            val workshopList = get(dataManager.getWorkshopList())!!.map {
                WorkshopListUnit(it.id, it.name)
            }

            val selectedWorkShop = try {
                get(dataManager.getWorkshopById(dataManager.getCurrentWorkshopId()))!!.id
            } catch (e: RuntimeException) {
                // Workshop hasn't been selected yet, or didn't found. Ignore it.
                workshopList[0].id
            }

            val workingShift = get(dataManager.getWorkingShift(selectedWorkShop))
            val workingShiftText = if (workingShift != -1)
                appContext.getString(R.string.working_shift, workingShift)
            else
                appContext.getString(R.string.working_shift_off)


            launch(UI) {
                view.apply {
                    setUserName(user.name)
                    setWorkingShift(workingShiftText)
                    setDate(date)
                    setWorkshopSpinner(workshopList)
                    setSelectedWorkshop(selectedWorkShop)
                    hidePreloader()
                }
            }
        }
    }

    override fun onWorkshopChange(selected: WorkshopListUnit) {
        smartLaunch {
            runBlocking {
                dataManager.setCurrentWorkshopId(selected.id)

                val workingShift = get(dataManager.getWorkingShift(selected.id))
                val workingShiftText = if (workingShift != -1)
                    appContext.getString(R.string.working_shift, workingShift)
                else
                    appContext.getString(R.string.working_shift_off)
                launch(UI) {
                    view.setWorkingShift(workingShiftText)
                }
            }
        }
    }

    override fun onActionBtnClick(button: FactoryMainView.Button) {
        view.navigateTo(when (button) {
            FactoryMainView.Button.WORKERS      -> FactoryMainView.Route.WORKERS
            FactoryMainView.Button.PRODUCTION   -> FactoryMainView.Route.PRODUCTION
            FactoryMainView.Button.QC           -> FactoryMainView.Route.QC
        })
    }

    override fun onSettingsClick() {
        view.navigateTo(FactoryMainView.Route.SETTINGS)
    }

    override fun onLogoutClick() {
        view.showLogoutDialog()
    }
}