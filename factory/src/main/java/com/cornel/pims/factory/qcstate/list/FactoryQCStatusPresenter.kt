package com.cornel.pims.factory.qcstate.list

import android.content.Context
import com.cornel.pims.factory.R
import com.cornel.pims.factory.common.QCListUnit
import com.cornel.pims.factory.core.AssignmentState
import com.cornel.pims.factory.core.DataManager
import com.cornel.pims.factory.core.proto.BasePresenter
import com.cornel.pims.factory.core.proto.BasePresenterImpl
import com.cornel.pims.factory.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import javax.inject.Inject


/**
 * Created by AlexGator on 09.03.18.
 */

interface FactoryQCStatusPresenter : BasePresenter {
    fun onCreate()
    fun onRefresh()
    fun onItemSelected(item: QCListUnit)
    fun onBackBtnPressed()
}

class FactoryQCStatusPresenterImpl(private val view: FactoryQCStatusView)
    : BasePresenterImpl(view as BaseView), FactoryQCStatusPresenter {

    companion object {
        val mapStateToHighlight = mapOf(
                AssignmentState.WAIT_QC to false,
                AssignmentState.REJECTED to true
        )
    }

    @Inject
    lateinit var appContext: Context

    @Inject
    lateinit var dataManager: DataManager

    override fun onCreate() {
        view.component.inject(this)
    }

    override fun doOnResumeAsync() {
        updateData()
    }

    override fun onRefresh() {
        smartLaunch {
            updateData()
        }
    }

    private fun updateData() {
        runBlocking {
            val list = get(dataManager.getQCAssignmentsByWorkshopId(dataManager.getCurrentWorkshopId()))!!
                    .map {
                        val product = get(dataManager.getProductById(it.productId))!!
                        val highlight = mapStateToHighlight[it.state] ?: false

                        QCListUnit(
                                it.id,
                                it.code,
                                product.name,
                                it.state?.value ?: "",
                                "",
                                highlight)
                    }

            info(list.toString())

            launch(UI) {
                view.showItems(list)
            }
        }
    }

    override fun onItemSelected(item: QCListUnit) {
        view.navigateToQCInfo(item.id)
    }

    override fun onBackBtnPressed() {
        view.finish()
    }

}