package com.cornel.pims.factory.workers.workplace

import dagger.Module
import dagger.Provides


/**
 * Created by AlexGator on 08.03.18.
 */

@Module
class FactoryWorkersWorkplaceListModule(
        private val activity: FactoryWorkersWorkplaceListActivity) {
    @FactoryWorkersWorkplaceListScope
    @Provides
    fun providePresenter(): FactoryWorkersWorkplaceListPresenter =
            FactoryWorkersWorkplaceListPresenterImpl(activity)
}
