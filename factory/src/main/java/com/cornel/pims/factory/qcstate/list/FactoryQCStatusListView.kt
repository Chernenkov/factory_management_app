package com.cornel.pims.factory.qcstate.list

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cornel.pims.factory.R
import com.cornel.pims.factory.common.QCListUnit
import com.cornel.pims.factory.core.AssignmentState
import com.cornel.pims.factory.core.app.cast
import com.cornel.pims.factory.core.proto.HasComponent
import kotlinx.android.synthetic.main.common_qc_status_list_item.view.*
import kotlinx.android.synthetic.main.common_recycler.view.*
import javax.inject.Inject

/**
 * Created by AlexGator on 12.04.18.
 */

interface FactoryQCStatusListView {
    fun setList(data: List<QCListUnit>)
    fun setFilter(filter: FactoryQCStatusView.Filter): FactoryQCStatusListFragment
}

class FactoryQCStatusListFragment: Fragment(), FactoryQCStatusListView {

    companion object {
        val stateTextMap = mapOf (
                AssignmentState.ARRIVED.value to R.string.factory_qc_state_arrived,
                AssignmentState.IN_PROGRESS.value to R.string.factory_qc_state_in_progress,
                AssignmentState.PAUSED.value to R.string.factory_qc_state_paused,
                AssignmentState.WAIT_REV.value to R.string.factory_qc_state_wait_revision,
                AssignmentState.REV_IN_PROGRESS.value to R.string.factory_qc_state_revision_in_progress,
                AssignmentState.WAIT_QC.value to R.string.factory_qc_state_wait_qc,
                AssignmentState.REJECTED.value to R.string.factory_qc_state_rejected,
                AssignmentState.WAIT_ACCEPT.value to R.string.factory_qc_state_wait_accept,
                AssignmentState.ACCEPT_STORAGE.value to R.string.factory_qc_state_accept_storage,
                AssignmentState.UNKNOWN.value to R.string.factory_qc_state_unknown
        )
    }

    @Inject
    lateinit var appContext: Context

    @Inject
    lateinit var presenter: FactoryQCStatusPresenter

    private var recycler: RecyclerView? = null

    private var data: List<QCListUnit> = emptyList()

    private var filter: FactoryQCStatusView.Filter = FactoryQCStatusView.Filter.ALL

    override fun setFilter(filter: FactoryQCStatusView.Filter): FactoryQCStatusListFragment {
        this.filter = filter
        return this
    }

    override fun setList(data: List<QCListUnit>) {
        this.data = data
        swapAdapter()
    }

    private fun swapAdapter() {
        val filtered = data.filter { when(filter) {
            FactoryQCStatusView.Filter.ALL -> true
            FactoryQCStatusView.Filter.REJECTED -> it.state == "rejected" || it.state == "waiting_for_revision"
            FactoryQCStatusView.Filter.AWAIT -> it.state == "waiting_for_technical_control"
        } }

        recycler?.swapAdapter(RecyclerAdapter(
                filtered, this::listener, appContext), false)
    }
    private fun listener(selected: QCListUnit) {
        presenter.onItemSelected(selected)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {

        recycler = inflater
                .inflate(R.layout.common_recycler, container, false) as RecyclerView
        (recycler as RecyclerView).layoutManager = LinearLayoutManager(context)
        (recycler as RecyclerView).addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        val swipeRefresh = SwipeRefreshLayout(context)
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        swipeRefresh.addView(recycler)

        return swipeRefresh
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        val componentActivity = cast<HasComponent<FactoryQCStatusComponent>>(activity)
                ?: throw RuntimeException("Activity is not initialized or unsupported")
        if (!this::appContext.isInitialized) {
            componentActivity.component.inject(this)
        }
    }

    class RecyclerAdapter(private val data: List<QCListUnit>,
                                 private val listener: (QCListUnit) -> Unit,
                                 private val context: Context)
        : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

        init {
            setHasStableIds(true)
        }

        inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
            var view = v
            val name = v.qcStatusItemName
            val code = v.qcStatusItemCode
            val shippingDate = v.qcStatusItemShippingDate
            val state = v.qcStatusItemState

            fun bind(item: QCListUnit, listener: (QCListUnit) -> Unit) {
                val stateResource = stateTextMap[item.state]
                val stateText = if (stateResource != null) {
                    context.getString(stateResource)
                } else item.state

                name.text = item.name
                code.text = item.code
                state.text = stateText

                val color = ContextCompat.getColor(context,
                        if (item.highlight) R.color.red800 else R.color.text_color)
                name.setTextColor(color)

                view.setOnClickListener { listener(item) }
            }
        }

        override fun getItemId(position: Int): Long = data[position].id.hashCode().toLong()

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = data[position]
            holder.bind(item, listener)
        }

        override fun getItemCount() = data.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.common_qc_status_list_item, parent, false)
            return ViewHolder(v)
        }
    }
}
