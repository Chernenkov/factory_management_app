package com.cornel.pims.factory.workers.add

import dagger.Module
import dagger.Provides


/**
 * Created by AlexGator on 08.03.18.
 */

@Module
class FactoryWorkerAddModule(
        private val activity: FactoryWorkerAddActivity) {
    @FactoryWorkerAddScope
    @Provides
    fun providePresenter(): FactoryWorkerAddPresenter =
            FactoryWorkerAddPresenterImpl(activity)
}
