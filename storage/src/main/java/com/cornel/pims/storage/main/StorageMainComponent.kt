package com.cornel.pims.storage.main

import dagger.Subcomponent
import javax.inject.Scope

/**
 * Created by AlexGator on 04.02.18.
 */

@Scope
annotation class StorageMainScope

@StorageMainScope
@Subcomponent(modules = arrayOf(StorageMainModule::class))
interface StorageMainComponent {
    fun inject(activity: StorageMainActivity)
    fun inject(presenter: StorageMainPresenterImpl)
}
