package com.cornel.pims.storage.acceptance.list

import android.content.Context
import com.cornel.pims.core.Formatter
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.AcceptanceListItem
import com.cornel.pims.storage.common.DocumentListUnit
import com.cornel.pims.storage.common.ItemListUnit
import com.cornel.pims.storage.core.DataManager
import com.cornel.pims.storage.core.proto.BasePresenter
import com.cornel.pims.storage.core.proto.BasePresenterImpl
import com.cornel.pims.storage.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import javax.inject.Inject

/**
 * Created by AlexGator on 24.02.18.
 */

interface StorageAcceptanceListPresenter : BasePresenter {
    fun onCreate()
    fun onProductRefresh()
    fun onProductSelected(item: AcceptanceListItem)
    fun onBackBtnClick()
    fun onFilterSelected(filter: StorageAcceptanceListView.Filter)
    fun onDocumentRefresh()
    fun onDocumentSelected(item: DocumentListUnit)
}

class StorageAcceptanceListPresenterImpl(private val view: StorageAcceptanceListView)
    : BasePresenterImpl(view as BaseView), StorageAcceptanceListPresenter {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var appContext: Context

    private var filterSelected = StorageAcceptanceListView.Filter.FACTORY

    override fun onCreate() {
        view.component.inject(this)
    }

    override fun doOnResumeAsync() {}

    override fun onProductSelected(item: AcceptanceListItem) {
        info("Item selected: $item")
        view.navigateToAcceptanceDocument(item.id)
    }

    override fun onDocumentSelected(item: DocumentListUnit) {
        info("Item selected: $item")
        view.navigateToAcceptRelocationDocument(item.id)
    }

    override fun onBackBtnClick() {
        info("Back button pressed")
        view.finish()
    }

    override fun onFilterSelected(filter: StorageAcceptanceListView.Filter) {
        when (filter) {
            StorageAcceptanceListView.Filter.FACTORY ->
                smartLaunch(action = this::loadAwaitFromFactory)
            StorageAcceptanceListView.Filter.STORAGE ->
                smartLaunch(action = this::loadAwaitFromStorage)
        }
    }

    override fun onDocumentRefresh() {
        smartLaunch {
            loadAwaitFromStorage()
        }
    }

    override fun onProductRefresh() {
        smartLaunch {
            loadAwaitFromFactory()
        }
    }

    private fun loadAwaitFromStorage() {
        var list : List<DocumentListUnit> = arrayListOf()
        runBlocking {
            val storageId = dataManager.getCurrentStorageId()
            val data = get(dataManager.getRelocationDocumentsListByStorageId(storageId))
                list = data.filter { it.elements.isNotEmpty() }
                        .map {
                    val element = it.elements[0]
                    DocumentListUnit(
                            it.id,
                            it.date,
                            element.productName,
                            element.productCode,
                            appContext.getString(R.string.measurement_form, Formatter.format(element.quantity), element.abbreviation))

                }
        }
        launch(UI) { view.showDocuments(list) }
    }

    private fun loadAwaitFromFactory() {
        var data : List<AcceptanceListItem> = arrayListOf()
        runBlocking {
            val storageId = dataManager.getCurrentStorageId()
            data = get(dataManager.getItemsAwaitAcceptanceByStorageId(storageId)).map {
                val product = get(dataManager.getProductById(it.productId))
                AcceptanceListItem(
                        it.id,
                        product.name,
                        product.id,
                        it.workplaceName,
                        appContext.getString(R.string.measurement_form, Formatter.format(it.quantity), it.abbreviation))
            }
        }
        launch(UI) { view.showProducts(data) }
    }
}