package com.cornel.pims.storage.stocktaking.document

import dagger.Subcomponent
import javax.inject.Scope

@Scope
annotation class StorageStocktakingDocumentScope

@StorageStocktakingDocumentScope
@Subcomponent(modules = arrayOf(StorageStocktakingDocumentModule::class))
interface StorageStocktakingDocumentComponent {
    fun inject(activity: StorageStocktakingDocumentActivity)
    fun inject(activity: StorageStocktakingDocumentPresenterImpl)
}