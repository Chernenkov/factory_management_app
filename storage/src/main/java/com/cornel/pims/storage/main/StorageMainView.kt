package com.cornel.pims.storage.main

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.cornel.pims.core.Barcode
import com.cornel.pims.core.BarcodeType
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.storage.R
import com.cornel.pims.storage.acceptance.list.StorageAcceptanceListActivity
import com.cornel.pims.storage.common.StorageListUnit
import com.cornel.pims.storage.core.app.app
import com.cornel.pims.storage.core.proto.BaseView
import com.cornel.pims.storage.information.InformationActivity
import com.cornel.pims.storage.relocation.arealist.StorageRelocationAreaListActivity
import com.cornel.pims.storage.shipping.orderlist.StorageShippingOrderListActivity
import com.cornel.pims.storage.stocktaking.arealist.StorageStocktakingAreaListActivity
import kotlinx.android.synthetic.main.common_main_spinner_list_item_dropdown.view.*
import kotlinx.android.synthetic.main.main_activity.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject

interface StorageMainView {
    // The action buttons which may be clicked by user in the current view
    enum class Button { ACCEPTANCE, TRANSFER, STOCKTAKING, SHIPPING }

    enum class Route { ACCEPTANCE, TRANSFER, STOCKTAKING, SHIPPING, SETTINGS }

    val component: StorageMainComponent

    /**
     * Simply shows a working shift
     * */
    fun setWorkingShift(text: String)

    /**
     * Simply shows current date
     * */
    fun setDate(text: String)

    /**
     * Simply shows a user name
     * */
    fun setUserName(name: String)

    /**
     * Fills the spinner with storage
     *
     * @param storageList a list of warehouses
     * */
    fun setStorageSpinner(storageList: List<StorageListUnit>)

    /**
     * Shows the quantity of items await reception
     * */
    fun showAwaitCount(text: String)

    fun hidePreloader()

    fun navigateTo(route: Route)
    fun setSelectedStorage(id: String): Boolean
    fun showError(e: Throwable)
    fun finish()
    fun finishAndRemoveTask()
    fun showLogoutDialog()
    fun hideAwaitCount()
}

class StorageMainActivity : BaseView(), StorageMainView {

    companion object {
        const val EXTRA_STORAGE_ID = "storage_id"
    }

    // Subcomponent will be instantiated only when we need to inject dependencies
    override val component by lazy { app.component.plus(StorageMainModule(this)) }

    @Inject
    override lateinit var presenter: StorageMainPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    private lateinit var spinnerValues: List<StorageListUnit>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        component.inject(this)

        barcodeHelper.register(this)

        setSupportActionBar(toolbar)

        val storageId = intent.getStringExtra(EXTRA_STORAGE_ID)

        presenter.onCreate(storageId)
        addListeners()
    }

    override fun onDestroy() {
        super.onDestroy()
        barcodeHelper.unregister(this)
    }

    override fun onBarcodeScan(barcode: Barcode) {
        if (barcode.type == BarcodeType.STORAGE) {
            setSelectedStorage(barcode.id)
        } else {
            super.onBarcodeScan(barcode)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_activity_appbar_menu, menu)
        return true
    }

    private fun addListeners() {
        btnAwait.onClick { presenter.onButtonPressed(StorageMainView.Button.ACCEPTANCE) }
        btnTransfer.onClick { presenter.onButtonPressed(StorageMainView.Button.TRANSFER) }
        btnStocktaking.onClick { presenter.onButtonPressed(StorageMainView.Button.STOCKTAKING) }
        btnShipping.onClick { presenter.onButtonPressed(StorageMainView.Button.SHIPPING) }

        storagesSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?,
                                        view: View?, position: Int, rowId: Long) {
                val selected = storagesSpinner.selectedItem as StorageListUnit
                presenter.onStorageChange(selected)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_settings -> presenter.onSettingsClick()
            R.id.action_logout -> presenter.onLogoutClick()
        }
        return true
    }

    override fun setWorkingShift(text: String) {
        textWorkingShift.text = text
    }

    override fun setDate(text: String) {
        textDate.text = text
    }

    override fun setUserName(name: String) {
        textUserName.text = name
    }

    override fun showAwaitCount(text: String) {
        textAwaitCount.text = text
        textAwaitCount.visibility = View.VISIBLE
    }

    override fun hideAwaitCount() {
        textAwaitCount.visibility = View.VISIBLE
    }

    override fun setSelectedStorage(id: String): Boolean {
        if (id == "") return false
        val selected = spinnerValues.find { it.id == id }
        val position = spinnerValues.indexOf(selected)
        return if (position != -1) {
            storagesSpinner.setSelection(position)
            true
        } else {
            showError(RuntimeException(
                    getString(R.string.main_error_cant_find_storage_with_id, id)),
                    false)
            false
        }
    }

    override fun setStorageSpinner(storageList: List<StorageListUnit>) {
        storagesSpinner.visibility = View.VISIBLE
        spinnerValues = storageList
        storagesSpinner.adapter = StorageSpinnerAdapter(this,
                R.layout.common_main_spinner_list_item,
                R.id.itemName,
                storageList)
    }

    override fun navigateTo(route: StorageMainView.Route) {
        val intent = when (route) {
            StorageMainView.Route.ACCEPTANCE -> intentFor<StorageAcceptanceListActivity>()
            StorageMainView.Route.TRANSFER -> intentFor<StorageRelocationAreaListActivity>()
            StorageMainView.Route.STOCKTAKING -> intentFor<StorageStocktakingAreaListActivity>()
            StorageMainView.Route.SHIPPING -> intentFor<StorageShippingOrderListActivity>()
            StorageMainView.Route.SETTINGS -> intentFor<InformationActivity>()
        }
        startActivity(intent)
    }

    override fun onBackPressed() {
        showLogoutDialog()
    }

    class StorageSpinnerAdapter(
            context: Context,
            viewResId: Int,
            viewTextResId: Int,
            val values: List<StorageListUnit>
    ) : ArrayAdapter<StorageListUnit>(context, viewResId, viewTextResId, values) {
        override fun getCount(): Int = values.size

        override fun getItem(position: Int) = values[position]

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = convertView ?: LayoutInflater.from(context)
                    .inflate(R.layout.common_main_spinner_list_item, parent, false)
            view.itemName.text = values[position].name
            return view
        }

        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = convertView ?: LayoutInflater.from(context)
                    .inflate(R.layout.common_main_spinner_list_item_dropdown, parent, false)
            view.itemName.text = values[position].name
            return view
        }
    }
}

