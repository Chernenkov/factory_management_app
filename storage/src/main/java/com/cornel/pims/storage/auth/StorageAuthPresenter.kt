package com.cornel.pims.storage.auth

import com.cornel.pims.storage.core.DataManager
import com.cornel.pims.storage.core.QError
import com.cornel.pims.storage.core.proto.BasePresenter
import com.cornel.pims.storage.core.proto.BasePresenterImpl
import com.cornel.pims.storage.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import javax.inject.Inject

/**
 * Created by AlexGator on 14.03.18.
 */

interface StorageAuthPresenter : BasePresenter {
    fun onCreate()
    fun onLogin(userLogin: String, userPass: String)
}

class StorageAuthPresenterImpl(private val view: StorageAuthView)
    : BasePresenterImpl(view as BaseView), StorageAuthPresenter {
    @Inject
    lateinit var dataManager: DataManager

    override fun onCreate() {
        view.component.inject(this)

        // Set the last login to the login input field
        smartLaunch {
            runBlocking {
                val login = dataManager.getLogin()
                launch(UI) {
                    view.setLogin(login)
                }
            }
        }
    }

    override fun doOnResumeAsync() {}

    override fun onLogin(userLogin: String, userPass: String) {
        smartLaunch {
            runBlocking {
                dataManager.setLogin(userLogin)
                dataManager.connect(userLogin, userPass)
                dataManager.getUser()
                val check = dataManager.getAppPermission()
                launch(UI) {
                    if (check) {
                        view.navigateToMainActivity()
                    } else {
                        view.showError(RuntimeException("Вы не имеете права работать в данном приложении. Обратитесь к начальнику производства"), false)
                    }
                }

            }
        }
    }

}