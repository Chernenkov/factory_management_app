package com.cornel.pims.storage.core

import com.cornel.pims.storage.core.app.App
import com.cornel.pims.storage.core.helper.APIHelper
import com.cornel.pims.storage.core.helper.SharedPrefHelper
import com.cornel.pims.storage.subscription.HandleStorageItemSubscription
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import kotlinx.coroutines.experimental.sync.Mutex
import kotlinx.coroutines.experimental.sync.withLock
import org.reactivestreams.Subscriber
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


interface DataManager {

    /* Init */
    suspend fun connect(login: String, password: String)

    suspend fun getUser(): QResponse<User> // Common
    suspend fun getAppPermission(): Boolean
    suspend fun getWorkingShift(storageId: String): QResponse<Int> // Common

    suspend fun getStorageList(): QResponse<List<Storage>> // Storage

    /**
     * Get storage selected by user
     * @throws RuntimeException when storage with the code is not found
     * */
    suspend fun getCurrentStorage(): QResponse<Storage> // Storage

    suspend fun getAreasByProductIdAndStorageId(productId: String, storageId: String): QResponse<List<AreaWithQuantity>>

    suspend fun getAreasByStorageId(id: String): QResponse<List<Area>> // Storage

    suspend fun getAreaById(id: String): QResponse<Area> // Storage

    suspend fun getProductList(): QResponse<List<Product>> // Storage
    suspend fun getProductById(id: String): QResponse<Product> // Storage

    suspend fun getStorageItemById(id: String): QResponse<StorageItem > // Storage
    suspend fun getStorageItemsByAreaId(id: String): QResponse<List<StorageItem>> // Storage

    suspend fun getOrders(): QResponse<List<Order>>  // Storage
    suspend fun getOrderById(id: String): QResponse<Order>

    suspend fun getRelocationDocumentsListByStorageId(id: String): QResponse<List<RelocationDocumentInfo>>
    suspend fun getRelocationDocumentById(id: String): QResponse<RelocationDocument>

    suspend fun getItemsAwaitAcceptanceByStorageId(id: String): QResponse<List<AcceptanceAssignment>>
    suspend fun getItemAwaitAcceptanceById(id: String): QResponse<AcceptanceAssignment?>


    /* Shared Preferences */
    fun getAuthUrl(): String // Common

    fun getApiUrl(): String // Common
    fun getCurrentStorageId(): String // Storage
    fun getLogin(): String // Common

    fun setAuthUrl(ip: String) // Common
    fun setApiUrl(port: String) // Common
    fun setCurrentStorageId(id: String) // Storage
    fun setLogin(login: String) // Common


    /* Mutations */

    suspend fun submitAcceptanceDocument(doc: AcceptanceDocument)
    suspend fun submitRelocationAcceptanceDocument(doc: RelocationAcceptanceDocument)
    suspend fun submitRelocationShipmentDocument(doc: RelocationShipmentDocument): String
    suspend fun submitShipmentDocument(doc: ShipmentDocument)
    suspend fun submitStocktakingDocument(doc: StocktakingDocument)

    /* Subscriptions */

    fun subscribeOnChangedStorageItem(onNext: () -> (Unit))
    fun stopStorageItemSubscription()
}

/*
 * TODO
 * Проработать логику если DM не возвращает корректных значений, определить поведение
 * в этом случае или показывать подробную ошибку на русском
*/

class DataManagerImpl(app: App) : DataManager {

    @Inject
    lateinit var sharedPrefHelper: SharedPrefHelper

    @Inject
    lateinit var apiHelper: APIHelper

    init {
        app.component.inject(this)
    }

    override suspend fun connect(login: String, password: String) {
        apiHelper.connect(login, password)
    }

    /* Cached data*/
    override suspend fun getStorageList(): QResponse<List<Storage>> {
        return apiHelper.getStorageList()
    }

    override suspend fun getCurrentStorage(): QResponse<Storage> {
        val id = getCurrentStorageId()
        val list = getStorageList()
        return QResponse(list.data?.find { it.id == id }
                ?: throw RuntimeException("Cannot find storage with code: $id"), list.error)
    }

    override suspend fun getAreasByProductIdAndStorageId(productId: String, storageId: String): QResponse<List<AreaWithQuantity>> {
        return apiHelper.getAreasByItemIdAndStorageId(productId, storageId)
    }

    override suspend fun getAreasByStorageId(id: String): QResponse<List<Area>> {
        return apiHelper.getAreasByStorageId(id)
    }

    override suspend fun getAreaById(id: String): QResponse<Area> {
        //TODO find the way search cached areas first
        return apiHelper.getAreaById(id)
    }

    override suspend fun getProductList(): QResponse<List<Product>> {
        return apiHelper.getProductList()
    }

    override suspend fun getProductById(id: String): QResponse<Product> {
        val list = getProductList()
        return QResponse(list.data?.find { it.id == id }
                ?: throw RuntimeException("Cannot find product with code=$id"), list.error)
    }

    override suspend fun getUser(): QResponse<User> {
        return apiHelper.getCurrentUser()
    }

    override suspend fun getAppPermission(): Boolean {
        return apiHelper.getAppPermission()
    }

    override fun getCurrentStorageId() = sharedPrefHelper.getStorageId()

    override fun setCurrentStorageId(id: String) = sharedPrefHelper.setStorageId(id)

    override suspend fun getStorageItemsByAreaId(id: String): QResponse<List<StorageItem>> {
        return apiHelper.getItemsByAreaId(id)
    }

    override suspend fun getStorageItemById(id: String): QResponse<StorageItem> {
        return apiHelper.getItemById(id)
    }

    override suspend fun getItemsAwaitAcceptanceByStorageId(id: String): QResponse<List<AcceptanceAssignment>> {
        return apiHelper.getAcceptanceAssignmentsByStorageId(id)
    }

    override suspend fun getItemAwaitAcceptanceById(id: String): QResponse<AcceptanceAssignment?> {
        return apiHelper.getAcceptanceAssignmentById(id)
    }

    override suspend fun getOrders(): QResponse<List<Order>> {
        return apiHelper.getOrderList()
    }

    override suspend fun getOrderById(id: String): QResponse<Order> {
        return apiHelper.getOrderById(id)
    }

    override suspend fun getWorkingShift(storageId: String): QResponse<Int> {
        return apiHelper.getCurrentShift(storageId)
    }

    override suspend fun submitAcceptanceDocument(doc: AcceptanceDocument) {
        apiHelper.submitAcceptanceDocument(doc)
    }

    override suspend fun submitRelocationAcceptanceDocument(doc: RelocationAcceptanceDocument) {
        apiHelper.submitRelocationAcceptanceDocument(doc)
    }

    override suspend fun submitRelocationShipmentDocument(doc: RelocationShipmentDocument): String {
        return apiHelper.submitRelocationShipmentDocument(doc)
    }

    override suspend fun submitStocktakingDocument(doc: StocktakingDocument) {
        apiHelper.submitStocktakingDocument(doc)
    }

    override suspend fun getRelocationDocumentsListByStorageId(id: String): QResponse<List<RelocationDocumentInfo>> {
        return apiHelper.getRelocationDocumentsByStorageId(id)
    }

    override suspend fun getRelocationDocumentById(id: String): QResponse<RelocationDocument> {
        return apiHelper.getRelocationDocumentById(id)
    }

    override suspend fun submitShipmentDocument(doc: ShipmentDocument) {
        apiHelper.submitShipmentDocument(doc)
    }

    override fun subscribeOnChangedStorageItem(onNext: () -> (Unit)) {
        apiHelper.subscribeStorageItem(onNext)
    }

    override fun stopStorageItemSubscription() {
        apiHelper.stopStorageItemSubscription()
    }

    override fun getLogin(): String = sharedPrefHelper.getLogin()

    override fun setAuthUrl(ip: String) = sharedPrefHelper.setAuthUrl(ip)

    override fun setApiUrl(port: String) = sharedPrefHelper.setApiUrl(port)

    override fun setLogin(login: String) = sharedPrefHelper.setLogin(login)

    override fun getAuthUrl() = sharedPrefHelper.getAuthUrl()

    override fun getApiUrl() = sharedPrefHelper.getApiUrl()
}
