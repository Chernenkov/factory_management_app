package com.cornel.pims.storage.relocation.productlist

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.ItemListUnit
import com.cornel.pims.storage.core.app.app
import com.cornel.pims.storage.core.proto.BaseView
import com.cornel.pims.storage.productinfo.StorageProductInfoActivity
import com.cornel.pims.storage.relocation.document.StorageRelocationDocumentActivity
import kotlinx.android.synthetic.main.common_item_list_item.view.*
import kotlinx.android.synthetic.main.common_list_with_go_back_button.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.noHistory
import org.jetbrains.anko.sdk25.coroutines.onClick
import javax.inject.Inject

/**
 * Created by KILLdon on 21.02.2018.
 */

interface StorageRelocationProductListView {
    val component: StorageRelocationProductListComponent
    fun showItems(data: List<ItemListUnit>)
    fun navigateToTransferDocument(itemId: String)
    fun showError(e: Throwable)
    fun setTitle(title: String)
    fun finish()
}

class StorageRelocationProductListActivity : BaseView(), StorageRelocationProductListView {

    companion object {
        const val EXTRA_AREA_ID = "zone_id"
        const val EXTRA_FROM_BARCODE = "from_barcode"
        const val REQUEST_CODE_DOCUMENT = 1
    }

    @Inject
    override lateinit var presenter: StorageRelocationProductListPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override val component by lazy { app.component.plus(StorageRelocationProductListModule(this)) }

    private var fromBarcode = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.common_list_with_go_back_button)
        component.inject(this)

        setSupportActionBar(toolbar)

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        val zoneId = intent.getStringExtra(EXTRA_AREA_ID)

        fromBarcode = intent.getBooleanExtra(EXTRA_FROM_BARCODE, false)

        presenter.onCreate(zoneId)

        addListeners()
    }

    override fun setTitle(title: String) {
        toolbar.title = title
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    private fun onRecyclerItemClick(item: ItemListUnit) {
        presenter.onItemSelected(item)
    }

    override fun showItems(data: List<ItemListUnit>) {
        recycler.swapAdapter(RecyclerAdapter(data, this, this::onRecyclerItemClick), false)
    }

    override fun navigateToTransferDocument(itemId: String) {
        if (fromBarcode) {
            startActivity(intentFor<StorageProductInfoActivity>(
                    StorageProductInfoActivity.EXTRA_ITEM_ID to itemId
            ).noHistory())
        } else {
            startActivityForResult(intentFor<StorageRelocationDocumentActivity>(
                    StorageRelocationDocumentActivity.EXTRA_ITEM_ID to itemId
            ), REQUEST_CODE_DOCUMENT)
        }
    }

    fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        goBackBtn.onClick {
            presenter.onBackButtonClick()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (resultCode) {
            Activity.RESULT_OK -> {
                showMessage(getString(R.string.relocation_product_list_success_send))
            }
            Activity.RESULT_CANCELED -> {
                showMessage(getString(R.string.relocation_product_list_failed_send))
            }
        }
    }

    class RecyclerAdapter(val data: List<ItemListUnit>,
                          private val context: Context,
                          private val listener: (ItemListUnit) -> Unit)
        : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

        inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
            var view = v
            val itemName: TextView = v.itemName
            val sku: TextView = v.itemCode
            val itemCount: TextView = v.itemCount

            fun bind(item: ItemListUnit, listener: (ItemListUnit) -> Unit) {
                val skuText = context.getString(R.string.relocation_product_list_code, item.code)
                itemName.text = item.name
                sku.text = skuText
                itemCount.text = item.quantityWithAbbreviation
                view.setOnClickListener { listener(item) }
            }
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = data[position]
            holder.bind(item, listener)
        }

        override fun getItemCount() = data.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.common_item_list_item, parent, false)
            return ViewHolder(v)
        }
    }

}