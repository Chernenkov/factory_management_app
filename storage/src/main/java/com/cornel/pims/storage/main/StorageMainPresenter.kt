package com.cornel.pims.storage.main

import android.content.Context
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.StorageListUnit
import com.cornel.pims.storage.core.DataManager
import com.cornel.pims.storage.core.QError
import com.cornel.pims.storage.core.QResponse
import com.cornel.pims.storage.core.proto.BasePresenter
import com.cornel.pims.storage.core.proto.BasePresenterImpl
import com.cornel.pims.storage.core.proto.BaseView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * Created by AlexGator on 04.02.18.
 */

interface StorageMainPresenter : BasePresenter {
    /**
     * Calls in the end of view onResume() method
     * */
    fun onCreate(storageId: String? = null)

    /**
     * Calls when a user clicks on the settings menu item
     * */
    fun onSettingsClick()

    /**
     * Calls when a user clicks on the logout menu item
     * */
    fun onLogoutClick()

    /**
     * Calls when a user select a storage from the spinner
     * */
    fun onStorageChange(storage: StorageListUnit)

    /**
     * Calls when a user clicks on the one of action buttons
     * */
    fun onButtonPressed(btn: StorageMainView.Button)
}

class StorageMainPresenterImpl(private val view: StorageMainView)
    : BasePresenterImpl(view as BaseView), StorageMainPresenter {
    @Inject
    lateinit var dataManager: DataManager
    @Inject
    lateinit var appContext: Context

    private var storageId: String? = null

    override fun onCreate(storageId: String?) {
        view.component.inject(this)
        this.storageId = storageId
    }

    override fun doOnResumeAsync() {
        runBlocking {
            val date = SimpleDateFormat("d MMM", Locale("ru")).format(Date())

            val storageList = get(dataManager.getStorageList())
                    .map { StorageListUnit(it.id, it.name) }

            val selectedNullable = if (storageId != null && storageList.find { it.id == storageId } != null)
                storageId else dataManager.getCurrentStorageId()

            val selected = selectedNullable ?: storageList[0].id

            val userName = get(dataManager.getUser()).name
            val wShift = get(dataManager.getWorkingShift(selected))

            val workingShift = if (wShift != -1)
                appContext.getString(R.string.main_working_shift, wShift)
            else
                appContext.getString(R.string.main_working_shift_off)

            launch(UI) {
                view.apply {
                    setStorageSpinner(storageList)
                    setWorkingShift(workingShift)
                    setDate(date)
                    setUserName(userName)
                    setSelectedStorage(selected)
                }
            }
            updateAwaitCount()
        }
    }

    private suspend fun updateAwaitCount() {
        //TODO
//        val count = dataManager.
//        val awaitCount = appContext
//                .getString(R.string.main_await_acceptance, count)
//        launch(UI) {
//            view.apply { if (count > 0) showAwaitCount(awaitCount) else hideAwaitCount() }
//        }
    }

    override fun onSettingsClick() {
        info("Settings clicked")
        view.navigateTo(StorageMainView.Route.SETTINGS)
    }

    override fun onLogoutClick() {
        view.showLogoutDialog()
    }

    override fun onStorageChange(storage: StorageListUnit) {
        info("Storage changed: $storage")

        smartLaunch {
            runBlocking {
                dataManager.setCurrentStorageId(storage.id)

                val wShift = get(dataManager.getWorkingShift(storage.id))
                val workingShift = if (wShift != -1)
                    appContext.getString(R.string.main_working_shift, wShift)
                else
                    appContext.getString(R.string.main_working_shift_off)

                launch(UI) {
                    view.apply {
                        setWorkingShift(workingShift)
                    }
                }

                updateAwaitCount()
            }
        }
    }

    override fun onButtonPressed(btn: StorageMainView.Button) {
        info("Button pressed: $btn")
        when (btn) {
            StorageMainView.Button.ACCEPTANCE -> view.navigateTo(StorageMainView.Route.ACCEPTANCE)
            StorageMainView.Button.TRANSFER -> view.navigateTo(StorageMainView.Route.TRANSFER)
            StorageMainView.Button.STOCKTAKING -> view.navigateTo(StorageMainView.Route.STOCKTAKING)
            StorageMainView.Button.SHIPPING -> view.navigateTo(StorageMainView.Route.SHIPPING)
        }
    }
}
