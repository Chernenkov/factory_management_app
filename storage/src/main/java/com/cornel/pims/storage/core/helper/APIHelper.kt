package com.cornel.pims.storage.core.helper

import android.util.Log
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.ApolloSubscriptionCall
import com.apollographql.apollo.api.Input
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.apollographql.apollo.exception.ApolloNetworkException
import com.apollographql.apollo.rx2.Rx2Apollo
import com.cornel.pims.core.api.GraphQLAPI
import com.cornel.pims.storage.core.*
import com.cornel.pims.storage.core.app.App
import com.cornel.pims.storage.mutation.*
import com.cornel.pims.storage.query.*
import com.cornel.pims.storage.subscription.HandleStorageItemSubscription
import com.cornel.pims.storage.type.*
import com.google.gson.JsonObject
import com.hosopy.actioncable.ActionCable
import com.hosopy.actioncable.Channel
import com.hosopy.actioncable.Consumer
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.ReplaySubject
import io.reactivex.subscribers.DisposableSubscriber
import kotlinx.coroutines.experimental.CompletableDeferred
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import okhttp3.HttpUrl
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import java.net.ConnectException
import java.net.URI
import javax.inject.Inject

interface APIHelper {
    fun connect(email: String, password: String)

    /* Queries */
    suspend fun getCurrentUser(): QResponse<User>
    suspend fun getAppPermission(): Boolean
    suspend fun getCurrentShift(storageId: String): QResponse<Int>

    suspend fun getProductList(): QResponse<List<Product>>
    suspend fun getStorageList(): QResponse<List<Storage>>
    suspend fun getStorageById(id: String): QResponse<Storage>
    suspend fun getAreasByItemIdAndStorageId(itemId: String, storageId: String): QResponse<List<AreaWithQuantity>>
    suspend fun getAreasByStorageId(id: String): QResponse<List<Area>>
    suspend fun getAreaById(id: String): QResponse<Area>
    suspend fun getItemById(id: String): QResponse<StorageItem>
    suspend fun getItemsByAreaId(id: String): QResponse<List<StorageItem>>
    suspend fun getOrderById(id: String): QResponse<Order>
    suspend fun getOrderList(): QResponse<List<Order>>
    suspend fun getRelocationDocumentsByStorageId(id: String): QResponse<List<RelocationDocumentInfo>>
    suspend fun getRelocationDocumentById(id: String): QResponse<RelocationDocument>
    suspend fun getAcceptanceAssignmentsByStorageId(id: String): QResponse<List<AcceptanceAssignment>>
    suspend fun getAcceptanceAssignmentById(id: String): QResponse<AcceptanceAssignment?>

    /* Mutations */
    suspend fun submitAcceptanceDocument(doc: AcceptanceDocument)

    suspend fun submitRelocationAcceptanceDocument(doc: RelocationAcceptanceDocument)
    suspend fun submitRelocationShipmentDocument(doc: RelocationShipmentDocument): String
    suspend fun submitShipmentDocument(doc: ShipmentDocument)
    suspend fun submitStocktakingDocument(doc: StocktakingDocument)

    /* Subscriptions */
    fun subscribeStorageItem(onNext: () -> (Unit))
    fun stopStorageItemSubscription()
}

class APIHelperImpl(app: App) : APIHelper {

    @Inject
    lateinit var sharedPrefHelper: SharedPrefHelper

    init{
        app.component.inject(this)
    }

    companion object {
        //TODO set 5, 3000
        const val LOG_TAG = "API_HELPER"
        const val RETRY_COUNT = 3
        const val RETRY_DELAY = 1000

        const val AUTH_ERROR = "Not authorized"
        const val TOKEN_ERROR = "Token"
    }

    private val authURL = HttpUrl.Builder()
            .scheme("http")
            .host("11.11.11.11")
            .port(1337)
            .addPathSegment("oauth")
            .addPathSegment("token")
            .build()!!

    private val apiURL = HttpUrl.Builder()
            .scheme("http")
            .host("11.11.11.11")
            .port(1338)
            .addPathSegment("graphql")
            .build()!!

    private val wsURL = "ws://11.11.11.11:1338/cable"

    private lateinit var graphQLAPI: GraphQLAPI
    private var consumer: Consumer? = null

    init {
        Log.d(LOG_TAG, "New APIHelper object created")
    }

    /**
     * Special function to rebuild private [GraphQLAPI] class.
     */
    private fun rebuildClient() {
        var i = 0
        // Retrying only on ConnectException
        while (i < RETRY_COUNT) {
            try {
                val email = sharedPrefHelper.getLogin()
                val password = sharedPrefHelper.getSecret()
                graphQLAPI = GraphQLAPI(apiURL, authURL, wsURL, email, password)
                Log.d(LOG_TAG, "Client rebuild")
                return
            } catch (e: ConnectException) {
                Log.e(LOG_TAG, "API client build failed", e)
                if (++i == RETRY_COUNT) throw e
            }
        }
    }

    /**
     * Special generic function which makes [RETRY_COUNT] retries
     * if error takes place while connecting.
     */
    private suspend fun <T> proceedQuery(requestFun: () -> Deferred<T>): T {
        var exception = Throwable()
        for (i in 0..RETRY_COUNT) {
            try {
                if (!this::graphQLAPI.isInitialized) {
                    rebuildClient()
                }
                return requestFun().await()
            } catch (e: ConnectException) {
                exception = e
                Log.e(LOG_TAG, "Request to Auth server failed, ${RETRY_COUNT - i} tries left", e)
                if (i == RETRY_COUNT) throw e
                delay(RETRY_DELAY)
                rebuildClient()
            } catch (e: ApolloNetworkException) {
                // Handles every exception during API server call, and invalid token too
                exception = e
                Log.e(LOG_TAG, "Request to API server failed, ${RETRY_COUNT - i} tries left", e)
                if (i == RETRY_COUNT) throw e
                delay(RETRY_DELAY)
                rebuildClient()
            }
        }

        throw exception
    }

    private fun <R, D> response(response: Response<R>, data: D?): QResponse<D> {
        return if (response.errors().isEmpty())
            QResponse(data, null)
        else
            QResponse(null, when {
                (response.errors().find { it.message()!!.startsWith(AUTH_ERROR) } != null) -> QError.AUTH
                (response.errors().find { it.message()!!.startsWith(TOKEN_ERROR) } != null) -> QError.TOKEN
                else -> QError.SERVER
            })
    }

    /*##################################################################################*/
    /*######################## INTERFACE IMPLEMENTATION ################################*/
    /*##################################################################################*/

    /* Authentication */
    override fun connect(email: String, password: String) {
        sharedPrefHelper.setLogin(email)
        sharedPrefHelper.setSecret(password)
        rebuildClient()
    }
    /*##################################################################################*/


    /* Common */
    override suspend fun getCurrentUser() = proceedQuery { getCurrentUserDefer() }

    private fun getCurrentUserDefer(): Deferred<QResponse<User>> {
        val deferredUser = CompletableDeferred<QResponse<User>>()

        graphQLAPI.client.query(GetCurrentUserQuery())
                .enqueue(object : ApolloCall.Callback<GetCurrentUserQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Could not get current_user object", e)
                        deferredUser.cancel(e)
                    }

                    override fun onResponse(response: Response<GetCurrentUserQuery.Data>) {
                        Log.i(LOG_TAG, "Current user query proceeded successfully")
                        Log.i(LOG_TAG, "Current user ID: ${response.data()?.current_user()?.id()}")
                        deferredUser.complete(response(response, if (!response.errors().isEmpty()) null else
                            User(response.data()!!.current_user()!!.id(),
                                    response.data()!!.current_user()!!.full_name())
                        ))
                    }
                })

        return deferredUser
    }

    override suspend fun getAppPermission() = proceedQuery { getAppPermissionDef() }

    private fun getAppPermissionDef() : Deferred<Boolean> {
        val deferredPermission = CompletableDeferred<Boolean>()

        graphQLAPI.client.query(GetAppPermissionQuery())
                .enqueue(object : ApolloCall.Callback<GetAppPermissionQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Could not get permission object", e)
                        deferredPermission.cancel(e)
                    }

                    override fun onResponse(response: Response<GetAppPermissionQuery.Data>) {
                        Log.i(LOG_TAG, "Permission query proceeded successfully")
                        val permission = response.data()?.current_user()?.permissions()?.find{ it.action() == "access" && it.secured_object() == "storage_app"}
                        if (permission == null) {
                            Log.i(LOG_TAG, "Permission to app was denied")
                        } else {
                            Log.i(LOG_TAG, "Permission to app was accepted")
                        }
                        deferredPermission.complete(permission != null)
                    }
                })

        return deferredPermission
    }

    override suspend fun getCurrentShift(storageId: String) = proceedQuery { getCurrentShiftDefer(storageId) }

    private fun getCurrentShiftDefer(storageId: String) : Deferred<QResponse<Int>> {
        val deferredShift = CompletableDeferred<QResponse<Int>>()

        graphQLAPI.client.query(GetCurrentShiftQuery(storageId))
                .enqueue(object : ApolloCall.Callback<GetCurrentShiftQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Could not get current_shift object", e)
                        deferredShift.cancel(e)
                    }

                    override fun onResponse(response: Response<GetCurrentShiftQuery.Data>) {
                        Log.i(LOG_TAG, "Current shift query proceeded successfully")
                        val number = response.data()?.current_shift()?.number()?.toInt()
                        if (number == null) {
                            Log.i(LOG_TAG, "Current shift was null")
                        } else {
                            Log.i(LOG_TAG, "Current shift: ${number}")
                        }
                        deferredShift.complete(response(response, if (!response.errors().isEmpty()) null else
                            number?: -1))
                    }
                })

        return deferredShift
    }

    override suspend fun getProductList() = proceedQuery(this::getProductListDefer)

    private fun getProductListDefer(): Deferred<QResponse<List<Product>>> {
        val deferredProductList = CompletableDeferred<QResponse<List<Product>>>()

        graphQLAPI.client.query(GetProductListQuery())
                .enqueue(object : ApolloCall.Callback<GetProductListQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Products list", e)
                        deferredProductList.cancel(e)
                    }

                    override fun onResponse(response: Response<GetProductListQuery.Data>) {
                        Log.i(LOG_TAG, "Received response: ${response.data()}")
                        deferredProductList.complete(response(response, if (!response.errors().isEmpty()) null else
                            response.data()!!.products()!!.map {
                            Product(it.id(), it.name(), it.code(), it.unit()!!.abbreviation())
                        }))
                    }
                })

        return deferredProductList
    }


    /**
     * Asks a private fun to proceed a query for List<Storage(code, name, code)>
     */
    override suspend fun getStorageList(): QResponse<List<Storage>> = proceedQuery(this::getStorageListDefer)

    /**
     * Proceeds query for List<Storage(code, name, code)>
     */
    private fun getStorageListDefer(): Deferred<QResponse<List<Storage>>> {
        val deferredStorageList = CompletableDeferred<QResponse<List<Storage>>>()

        graphQLAPI.client.query(GetStorageListQuery())
                .enqueue(object : ApolloCall.Callback<GetStorageListQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Storage list", e)
                        deferredStorageList.cancel(e)
                    }

                    override fun onResponse(response: Response<GetStorageListQuery.Data>) {
                        Log.i(LOG_TAG, "Received response: ${response.data()?.storages()}")
                        deferredStorageList.complete(response(response, if (!response.errors().isEmpty()) null else
                            response.data()?.storages()?.map {
                            Storage(it.id(), it.name(), it.code())
                        }))
                    }
                })

        return deferredStorageList
    }

    /**
     * Suspending fun to get the current storage values in Storage(code, name, code)
     * data-class. Calls a special private query-function to GraphQL Apollo-generated class.
     */
    override suspend fun getStorageById(id: String) = proceedQuery { getStorageByIdDefer(id) }

    /**
     * Private fun to proceed a request to GraphQL
     */
    private fun getStorageByIdDefer(id: String): Deferred<QResponse<Storage>> {
        val deferredStorage = CompletableDeferred<QResponse<Storage>>()

        graphQLAPI.client.query(GetStorageByIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetStorageByIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query storage by code", e)
                        deferredStorage.cancel(e)
                    }

                    override fun onResponse(response: Response<GetStorageByIdQuery.Data>) {
                        Log.i(LOG_TAG, "Received response: ${response.data()?.storage()}")
                        deferredStorage.complete(response(response, if (!response.errors().isEmpty()) null else
                            Storage(response.data()!!.storage()!!.id(),
                                response.data()!!.storage()!!.name(),
                                response.data()!!.storage()!!.code())))
                    }

                })

        return deferredStorage
    }

    /**
     * Asks a private fun to proceed a reques for List<Area(code, name, code, storageId)>
     */
    override suspend fun getAreasByItemIdAndStorageId(itemId: String, storageId: String) = proceedQuery { getAreasByItemIdAndStorageIdDefer(itemId, storageId) }

    /**
     * Proceeds request to get List<Area(code, name, code, storageId)>
     */
    private fun getAreasByItemIdAndStorageIdDefer(itemId: String, storageId: String): Deferred<QResponse<List<AreaWithQuantity>>> {
        val deferredAreasList = CompletableDeferred<QResponse<List<AreaWithQuantity>>>()

        graphQLAPI.client.query(GetAreaListByItemIdAndStorageIdQuery(itemId, storageId))
                .enqueue(object : ApolloCall.Callback<GetAreaListByItemIdAndStorageIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Areas by Storage ID", e)
                        deferredAreasList.cancel(e)
                    }

                    override fun onResponse(response: Response<GetAreaListByItemIdAndStorageIdQuery.Data>) {
                        Log.i(LOG_TAG, "Received response: ${response.data()?.storage_items()}")
                        deferredAreasList.complete(response(response, if (!response.errors().isEmpty()) null else
                            response.data()!!.storage_items()!!.map {
                            AreaWithQuantity(
                                    it.area().id(),
                                    it.area().name(),
                                    it.quantity())
                        }))
                    }
                })

        return deferredAreasList
    }

    /**
     * Asks a private fun to proceed a reques for List<Area(code, name, code, storageId)>
     */
    override suspend fun getAreasByStorageId(id: String) = proceedQuery { getAreasByStorageIdDefer(id) }

    /**
     * Proceeds request to get List<Area(code, name, code, storageId)>
     */
    private fun getAreasByStorageIdDefer(id: String): Deferred<QResponse<List<Area>>> {
        val deferredAreasList = CompletableDeferred<QResponse<List<Area>>>()

        graphQLAPI.client.query(GetAreaListByStorageIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetAreaListByStorageIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Areas by Storage ID", e)
                        deferredAreasList.cancel(e)
                    }

                    override fun onResponse(response: Response<GetAreaListByStorageIdQuery.Data>) {
                        Log.i(LOG_TAG, "Received response: ${response.data()?.storage()}")
                        deferredAreasList.complete(response(response, if (!response.errors().isEmpty()) null else
                            response.data()!!.storage()!!.areas()!!.map {
                            Area(it.id(),
                                    it.name(),
                                    it.code(),
                                    it.storage().id(),
                                    it.storage_items_count()!!.toInt())
                        }))
                    }
                })

        return deferredAreasList
    }

    /**
     * Asks a private fun to proceed a query for Area(code, name, code, storageId)
     */
    override suspend fun getAreaById(id: String) = proceedQuery { getAreaByIdDefer(id) }

    /**
     * Proceeds a query for area{code,name,code,storage{code}}
     */
    private fun getAreaByIdDefer(id: String): Deferred<QResponse<Area>> {
        val deferredArea = CompletableDeferred<QResponse<Area>>()

        graphQLAPI.client.query(GetAreaByIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetAreaByIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Area by ID", e)
                        deferredArea.cancel(e)
                    }

                    override fun onResponse(response: Response<GetAreaByIdQuery.Data>) {
                        Log.i(LOG_TAG, "Received response: ${response.data()?.area()}")
                        deferredArea.complete(response(response, if (!response.errors().isEmpty()) null else
                            Area(response.data()!!.area()!!.id(),
                                response.data()!!.area()!!.name(),
                                response.data()!!.area()!!.code(),
                                response.data()!!.area()!!.storage().id(),
                                response.data()!!.area()!!.storage_items_count()?.toInt() ?: 0)))
                    }
                })

        return deferredArea
    }

    override suspend fun getItemById(id: String) = proceedQuery { getItemByIdQuery(id) }

    // TODO make out with partner name
    private fun getItemByIdQuery(id: String): Deferred<QResponse<StorageItem>> {
        val deferredItem = CompletableDeferred<QResponse<StorageItem>>()

        graphQLAPI.client.query(GetStorageItemByIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetStorageItemByIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Item by ID", e)
                        deferredItem.cancel(e)
                    }

                    override fun onResponse(response: Response<GetStorageItemByIdQuery.Data>) {
                        Log.i(LOG_TAG, "Received response: ${response.data()}")
                        deferredItem.complete(response(response, if (!response.errors().isEmpty()) null else {
                            val item = response.data()!!.storage_item()!!
                            StorageItem(item.id(),
                                item.quantity(),
                                item.product().id(),
                                item.area().id(),
                                //TODO receive partnerName
                                "",
                                "",
                                item.product().unit()!!.abbreviation()
                        )}))
                    }
                })

        return deferredItem
    }


    override suspend fun getItemsByAreaId(id: String) = proceedQuery { getItemsByAreaIdDefer(id) }

    //TODO get on with 'code' in Product
    private fun getItemsByAreaIdDefer(id: String): Deferred<QResponse<List<StorageItem>>> {
        val deferredItemsList = CompletableDeferred<QResponse<List<StorageItem>>>()

        graphQLAPI.client.query(GetStorageItemListByAreaIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetStorageItemListByAreaIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Items by Area ID", e)
                        deferredItemsList.cancel(e)
                    }

                    override fun onResponse(response: Response<GetStorageItemListByAreaIdQuery.Data>) {
                        Log.v(LOG_TAG, "Received response: ${response.data()?.area()}")
                        deferredItemsList.complete(response(response, if (!response.errors().isEmpty()) null else
                            response.data()!!.area()!!.storage_items()!!.map {
                                StorageItem(it.id(),
                                        it.quantity(),
                                        it.product().id(),
                                        it.area().id(),
                                        //TODO receive partnerName
                                        "",
                                        "",
                                        it.product().unit()!!.abbreviation())
                            }))
                    }
                })

        return deferredItemsList
    }

    override suspend fun getOrderById(id: String) = proceedQuery { getOrderByIdDef(id) }

    private fun getOrderByIdDef(id: String): Deferred<QResponse<Order>> {
        val deferredOrder = CompletableDeferred<QResponse<Order>>()

        graphQLAPI.client.query(GetOrderByIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetOrderByIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query list of all Orders", e)
                        deferredOrder.cancel(e)
                    }

                    override fun onResponse(response: Response<GetOrderByIdQuery.Data>) {
                        Log.i(LOG_TAG, "Received order: ${response.data()}")
                        deferredOrder.complete(response(response,
                                if (!response.errors().isEmpty()) null else {
                                    val order = response.data()!!.order()!!
                                    Order(
                                        order.id(),
                                        order.number() ?: 0,
                                        order.partner()?.id() ?: "",
                                        order.partner()?.name() ?: "",
                                        order.elements()?.map {
                                            OrderElement(
                                                    it.product().id(),
                                                    it.product().name(),
                                                    it.shipment_date()?: "",
                                                    it.product().unit()!!.abbreviation(),
                                                    it.quantity())
                                        } ?: emptyList()
                                )
                    }
                        ))
                    }
                })

        return deferredOrder
    }

    override suspend fun getOrderList(): QResponse<List<Order>> = proceedQuery(this::getAllOrdersDefer)

    private fun getAllOrdersDefer(): Deferred<QResponse<List<Order>>> {
        val deferredOrderList = CompletableDeferred<QResponse<List<Order>>>()

        graphQLAPI.client.query(GetOrderListQuery())
                .enqueue(object : ApolloCall.Callback<GetOrderListQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query list of all Orders", e)
                        deferredOrderList.cancel(e)
                    }

                    override fun onResponse(response: Response<GetOrderListQuery.Data>) {
                        Log.i(LOG_TAG, "List of Orders queried successfully")
                        Log.i(LOG_TAG, "Received list: ${response.data()?.orders()}")
                        deferredOrderList.complete(response(response, if (!response.errors().isEmpty()) null else
                            response.data()!!.orders()!!.map {
                            Order(it.id(),
                                    it.number() ?: 0,
                                    it.partner()?.id() ?: "",
                                    it.partner()?.name() ?: "",
                                    it.elements()?.map {
                                        OrderElement(
                                                it.product().id(),
                                                it.product().name(),
                                                "",
                                                it.product().unit()!!.abbreviation(),
                                                it.quantity())
                                    } ?: emptyList())
                        }))
                    }
                })

        return deferredOrderList
    }

    override suspend fun getRelocationDocumentsByStorageId(id: String) = proceedQuery { getRelocationDocumentsDefer(id) }

    private fun getRelocationDocumentsDefer(id: String): Deferred<QResponse<List<RelocationDocumentInfo>>> {
        val deferredRelocationDocumentsList = CompletableDeferred<QResponse<List<RelocationDocumentInfo>>>()

        graphQLAPI.client.query(GetRelocationDocumentsByStorageIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetRelocationDocumentsByStorageIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Relocation Documents by Storage ID", e)
                        deferredRelocationDocumentsList.cancel(e)
                    }

                    override fun onResponse(response: Response<GetRelocationDocumentsByStorageIdQuery.Data>) {
                        Log.i(LOG_TAG, "Received response: ${response.data()?.relocation_shipment_documents()}")
                        deferredRelocationDocumentsList.complete(response(response, if (!response.errors().isEmpty()) null else
                            response.data()!!.relocation_shipment_documents()!!.map {
                            RelocationDocumentInfo(
                                    it.id(),
                                    it.date(),
                                    it.elements()!!.map {
                                        val product = it.product()
                                        RelocationDocumentElement(
                                                product.name(),
                                                product.code(),
                                                product.unit()!!.abbreviation(),
                                                it.quantity())
                                    })
                        }))
                    }
                })

        return deferredRelocationDocumentsList
    }

    override suspend fun getRelocationDocumentById(id: String) = proceedQuery { getRelocationDocumentDefer(id) }

    private fun getRelocationDocumentDefer(id: String): Deferred<QResponse<RelocationDocument>> {
        val deferredRelocationDocument = CompletableDeferred<QResponse<RelocationDocument>>()

        graphQLAPI.client.query(GetRelocationDocumentByIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetRelocationDocumentByIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to query Relocation Documents by Storage ID", e)
                        deferredRelocationDocument.cancel(e)
                    }

                    override fun onResponse(response: Response<GetRelocationDocumentByIdQuery.Data>) {
                        Log.i(LOG_TAG, "Received response: ${response.data()?.relocation_shipment_document()}")
                        deferredRelocationDocument.complete(response(response,
                                if (!response.errors().isEmpty()) null else {
                                    val document = response.data()!!.relocation_shipment_document()!!
                                    val element = document.elements()!![0]
                                    RelocationDocument(
                                        document.code(),
                                        document.storage()!!.id(),
                                        document.storage()!!.name(),
                                        element.area()!!.storage().id(),
                                        element.area()!!.storage().name(),
                                        element.area()!!.id(),
                                        element.area()!!.name(),
                                        document.user()!!.full_name(),
                                        Product(
                                                element.product().id(),
                                                element.product().name(),
                                                element.product().code(),
                                                element.product().unit()!!.abbreviation()),
                                        element.quantity())}
                        ))
                    }
                })

        return deferredRelocationDocument
    }

    /* Mutations */
    override suspend fun submitStocktakingDocument(doc: StocktakingDocument) {
        val documentId = proceedQuery { createStocktakingDocumentDefer(doc) }
        proceedQuery { acceptStocktakingDocumentDefer(documentId) }
    }

    private fun createStocktakingDocumentDefer(doc: StocktakingDocument): Deferred<String> {
        val elements = Input.fromNullable(doc.elements.map {
            StocktakingElementInputType.builder()
                    .quantity(it.quantity)
                    .product_id(it.productId)
                    .build()
        })

        val deferredDocumentId = CompletableDeferred<String>()

        graphQLAPI.client.mutate(CreateStocktakingDocumentMutation(doc.date, doc.areaId, elements))
                .enqueue(object : ApolloCall.Callback<CreateStocktakingDocumentMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to create Stocktaking document", e)
                        deferredDocumentId.cancel(e)
                    }

                    override fun onResponse(response: Response<CreateStocktakingDocumentMutation.Data>) {
                        Log.i(LOG_TAG, "Stocktaking Document created successfully")
                        Log.i(LOG_TAG, "Created Stocktaking Document ID: ${response.data()!!.create_stocktaking_document().id()}")
                        deferredDocumentId.complete(response.data()!!.create_stocktaking_document().id())
                    }
                })

        return deferredDocumentId
    }

    private fun acceptStocktakingDocumentDefer(id: String): Deferred<Any?> {
        val deferredDocumentId = CompletableDeferred<String>()

        graphQLAPI.client.mutate(AcceptStocktakingDocumentMutation(id))
                .enqueue(object : ApolloCall.Callback<AcceptStocktakingDocumentMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to accept created Stocktaking Document", e)
                        deferredDocumentId.cancel(e)
                    }

                    override fun onResponse(response: Response<AcceptStocktakingDocumentMutation.Data>) {
                        Log.i(LOG_TAG, "Created Stocktaking Document accepted successfully")
                        Log.i(LOG_TAG, "Stocktaking Document acceptance ID: ${response.data()!!.accept_stocktaking_document().id()}")
                        deferredDocumentId.complete(response.data()!!.accept_stocktaking_document().id())
                    }
                })

        return deferredDocumentId
    }


    override suspend fun submitAcceptanceDocument(doc: AcceptanceDocument) {
        val documentId = proceedQuery { createAcceptanceDocumentDefer(doc) }
        proceedQuery { acceptAcceptanceDocumentDefer(documentId) }
    }

    private fun createAcceptanceDocumentDefer(doc: AcceptanceDocument): Deferred<String> {
        val elements = Input.fromNullable(doc.elements.map {
            AcceptanceElementInputType.builder()
                    .product_id(it.productId)
                    .production_assignment_id(it.productAssignmentId)
                    .quantity(it.quantity)
                    .build()
        })

        val partnerId = Input.fromNullable(doc.partnerId)

        val deferredDocumentId = CompletableDeferred<String>()

        graphQLAPI.client.mutate(CreateAcceptanceDocumentMutation(
                doc.date,
                partnerId,
                doc.areaId,
                elements))
                .enqueue(object : ApolloCall.Callback<CreateAcceptanceDocumentMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to create Acceptance Document", e)
                        deferredDocumentId.cancel(e)
                    }

                    override fun onResponse(response: Response<CreateAcceptanceDocumentMutation.Data>) {
                        Log.i(LOG_TAG, "Acceptance Document created successfully")
                        Log.i(LOG_TAG, "Created Acceptance Document ID: ${response.data()!!.create_acceptance_document().id()}")
                        deferredDocumentId.complete(response.data()!!.create_acceptance_document().id())
                    }
                })

        return deferredDocumentId
    }

    private fun acceptAcceptanceDocumentDefer(id: String): Deferred<Any?> {
        val deferredDocumentId = CompletableDeferred<String>()

        graphQLAPI.client.mutate(AcceptAcceptanceDocumentMutation(id))
                .enqueue(object : ApolloCall.Callback<AcceptAcceptanceDocumentMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to accept created Acceptance Document", e)
                        deferredDocumentId.cancel(e)
                    }

                    override fun onResponse(response: Response<AcceptAcceptanceDocumentMutation.Data>) {
                        Log.i(LOG_TAG, "Created Acceptance Document accepted successfully")
                        Log.i(LOG_TAG, "Acceptance Document acceptance ID: ${response.data()!!.accept_acceptance_document().id()}")
                        deferredDocumentId.complete(response.data()!!.accept_acceptance_document().id())
                    }
                })

        return deferredDocumentId
    }


    override suspend fun submitRelocationAcceptanceDocument(doc: RelocationAcceptanceDocument) {
        val documentId = proceedQuery { createRelocationAcceptanceDocumentDefer(doc) }
        proceedQuery { acceptRelocationAcceptanceDocumentDefer(documentId) }
    }

    private fun createRelocationAcceptanceDocumentDefer(doc: RelocationAcceptanceDocument): Deferred<String> {
        val deferredDocumentId = CompletableDeferred<String>()

        val elements = Input.fromNullable(doc.elements.map {
            RelocationAcceptanceElementInputType.builder()
                    .product_id(it.productId)
                    .area_id(it.targetAreaId)
                    .quantity(it.quantity)
                    .build()
        })

        graphQLAPI.client.mutate(CreateRelocationAcceptanceDocumentMutation(
                doc.date,
                doc.parentId,
                elements))
                .enqueue(object : ApolloCall.Callback<CreateRelocationAcceptanceDocumentMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to create Relocation Acceptance Document", e)
                        deferredDocumentId.cancel(e)
                    }

                    override fun onResponse(response: Response<CreateRelocationAcceptanceDocumentMutation.Data>) {
                        Log.i(LOG_TAG, "Relocation Acceptance Document created successfully")
                        Log.i(LOG_TAG, "Created Relocation Acceptance Document ID: ${response.data()!!.create_relocation_acceptance_document().id()}")
                        deferredDocumentId.complete(response.data()!!.create_relocation_acceptance_document().id())
                    }
                })

        return deferredDocumentId
    }

    private fun acceptRelocationAcceptanceDocumentDefer(id: String): Deferred<String> {
        val deferredDocumentId = CompletableDeferred<String>()

        graphQLAPI.client.mutate(AcceptRelocationAcceptanceDocumentMutation(id))
                .enqueue(object : ApolloCall.Callback<AcceptRelocationAcceptanceDocumentMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to accept Relocation Acceptance Document", e)
                        deferredDocumentId.cancel(e)
                    }

                    override fun onResponse(response: Response<AcceptRelocationAcceptanceDocumentMutation.Data>) {
                        Log.i(LOG_TAG, "Created Relocation Acceptance Document accepted successfully")
                        Log.i(LOG_TAG, "Relocation Acceptance Document acceptance document ID: ${response.data()!!.accept_relocation_acceptance_document().id()}")
                        deferredDocumentId.complete(response.data()!!.accept_relocation_acceptance_document().id())
                    }
                })

        return deferredDocumentId
    }

    override suspend fun submitRelocationShipmentDocument(doc: RelocationShipmentDocument): String {
        val documentId = proceedQuery { createRelocationShipmentDocumentDefer(doc) }
        proceedQuery { acceptRelocationShipmentDocumentDefer(documentId) }
        return documentId
    }

    private fun createRelocationShipmentDocumentDefer(doc: RelocationShipmentDocument): Deferred<String> {
        val deferredDocumentId = CompletableDeferred<String>()

        val elements = Input.fromNullable(doc.elements.map {
            RelocationShipmentElementInputType.builder()
                    .product_id(it.productId)
                    .area_id(it.sourceAreaId)
                    .quantity(it.quantity)
                    .build()
        })

        graphQLAPI.client.mutate(CreateRelocationShipmentDocumentMutation(
                doc.storageId,
                doc.date,
                elements))
                .enqueue(object : ApolloCall.Callback<CreateRelocationShipmentDocumentMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to update Relocation Shipment Document by ID", e)
                        deferredDocumentId.cancel(e)
                    }

                    override fun onResponse(response: Response<CreateRelocationShipmentDocumentMutation.Data>) {
                        Log.i(LOG_TAG, "Relocation Shipment Document updated successfully")
                        Log.i(LOG_TAG, "Relocation Shipment Document updating ID: ${response.data()!!.create_relocation_shipment_document().id()}")
                        deferredDocumentId.complete(response.data()!!.create_relocation_shipment_document().id())
                    }
                })

        return deferredDocumentId
    }

    private fun acceptRelocationShipmentDocumentDefer(id: String): Deferred<String> {
        val deferredDocumentId = CompletableDeferred<String>()

        graphQLAPI.client.mutate(AcceptRelocationShipmentDocumentMutation(id))
                .enqueue(object : ApolloCall.Callback<AcceptRelocationShipmentDocumentMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to accept Relocation Shipment Document", e)
                        deferredDocumentId.cancel(e)
                    }

                    override fun onResponse(response: Response<AcceptRelocationShipmentDocumentMutation.Data>) {
                        Log.i(LOG_TAG, "Created Relocation Shipment Document accepted successfully")
                        Log.i(LOG_TAG, "Relocation Shipment Document acceptance document ID: ${response.data()!!.accept_relocation_shipment_document().id()}")
                        deferredDocumentId.complete(response.data()!!.accept_relocation_shipment_document().id())
                    }
                })

        return deferredDocumentId
    }


    override suspend fun submitShipmentDocument(doc: ShipmentDocument) {
        val shipmentDocumentId = proceedQuery { createShipmentDocumentDefer(doc) }
        proceedQuery { acceptShipmentDocumentDefer(shipmentDocumentId) }
    }

    private fun createShipmentDocumentDefer(doc: ShipmentDocument): Deferred<String> {
        val deferredDocumentId = CompletableDeferred<String>()

        val partnerId = Input.fromNullable(doc.partnerId)

        val elements = Input.fromNullable(doc.elements.map {
            ShipmentElementInputType.builder()
                    .product_id(it.productId)
                    .area_id(it.areaId)
                    .quantity(it.quantity)
                    .build()
        })

        graphQLAPI.client.mutate(CreateShipmentDocumentMutation(partnerId,
                doc.date,
                elements))
                .enqueue(object : ApolloCall.Callback<CreateShipmentDocumentMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to create Shipment Document", e)
                        deferredDocumentId.cancel(e)
                    }

                    override fun onResponse(response: Response<CreateShipmentDocumentMutation.Data>) {
                        Log.i(LOG_TAG, "Shipment Document created successfully")
                        Log.i(LOG_TAG, "Created Shipment Document ID: ${response.data()!!.create_shipment_document().id()}")
                        deferredDocumentId.complete(response.data()!!.create_shipment_document().id())
                    }
                })

        return deferredDocumentId
    }

    private fun acceptShipmentDocumentDefer(id: String): Deferred<String> {
        val deferredDocumentId = CompletableDeferred<String>()

        graphQLAPI.client.mutate(AcceptShipmentDocumentMutation(id))
                .enqueue(object : ApolloCall.Callback<AcceptShipmentDocumentMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Failed to accept Shipment Document", e)
                        deferredDocumentId.cancel(e)
                    }

                    override fun onResponse(response: Response<AcceptShipmentDocumentMutation.Data>) {
                        Log.i(LOG_TAG, "Shipment Document accepted successfully")
                        Log.i(LOG_TAG, "Shipment Document acceptance document ID: ${response.data()!!.accept_shipment_document().id()}")
                        deferredDocumentId.complete(response.data()!!.accept_shipment_document().id())
                    }
                })

        return deferredDocumentId
    }

    override suspend fun getAcceptanceAssignmentsByStorageId(id: String): QResponse<List<AcceptanceAssignment>> {
        return proceedQuery { getAcceptanceAssignmentsByStorageIdDeferred(id) }
    }

    private fun getAcceptanceAssignmentsByStorageIdDeferred(id: String)
            : Deferred<QResponse<List<AcceptanceAssignment>>> {
        val defer = CompletableDeferred<QResponse<List<AcceptanceAssignment>>>()

        graphQLAPI.client.query(GetAcceptanceAssignmentsByStorageIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetAcceptanceAssignmentsByStorageIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Query failed", e)
                        defer.cancel(e)
                    }

                    override fun onResponse(response: Response<GetAcceptanceAssignmentsByStorageIdQuery.Data>) {
                        Log.i(LOG_TAG, "Data queried successfully")
                        defer.complete(response(response, if (!response.errors().isEmpty()) null else
                            response.data()?.production_assignments()?.map {
                                AcceptanceAssignment(
                                        it.id(),
                                        it.code(),
                                        "",
                                        null,
                                        it.quantity(),
                                        it.route_map().product().id(),
                                        it.route_map().product().unit()?.abbreviation() ?: "шт",
                                        it.production_date() ?: "",
                                        it.current_workplace()?.name() ?: "",
                                        it.current_workplace()?.workshop()?.head_users()?.map{
                                            it.full_name()
                                        } ?: emptyList(),
                                        emptyList()
                                )
                            } ?: emptyList()))
                    }
                })

        return defer
    }


    override suspend fun getAcceptanceAssignmentById(id: String): QResponse<AcceptanceAssignment?> {
        return proceedQuery { getAcceptanceAssignmentByIdIdDeferred(id) }
    }

    private fun getAcceptanceAssignmentByIdIdDeferred(id: String)
            : Deferred<QResponse<AcceptanceAssignment?>> {
        val defer = CompletableDeferred<QResponse<AcceptanceAssignment?>>()

        graphQLAPI.client.query(GetAcceptanceAssignmentByIdQuery(id))
                .enqueue(object : ApolloCall.Callback<GetAcceptanceAssignmentByIdQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        Log.e(LOG_TAG, "Query failed", e)
                        defer.cancel(e)
                    }

                    override fun onResponse(response: Response<GetAcceptanceAssignmentByIdQuery.Data>) {
                        Log.i(LOG_TAG, "Data queried successfully")
                        defer.complete(response(response, if (!response.errors().isEmpty()) null else {
                            val assignment = response.data()!!.production_assignment()
                            if (assignment == null)
                                null
                            else {
                                val unit = assignment.route_map().product().unit()!!

                                AcceptanceAssignment(
                                        assignment.id(),
                                        assignment.code(),
                                        assignment.state(),
                                        assignment.deleted_at(),
                                        assignment.quantity(),
                                        assignment.route_map().product().id(),
                                        unit.abbreviation(),
                                        assignment.production_date() ?: "",
                                        assignment.current_workplace()?.name() ?: "",
                                        assignment.current_workplace()?.workshop()?.head_users()?.map {
                                            it.full_name()
                                        } ?: emptyList(),
                                        assignment.factory_events()?.map {
                                            HistoryUnit(it.message(), it.date())
                                        } ?: emptyList()
                                )
                            }
                        }))
                    }
                })

        return defer
    }

    override fun subscribeStorageItem(onNext: () -> (Unit)) {
        subscribeStorageItemDef(onNext)
    }

    override fun stopStorageItemSubscription() {
        consumer?.disconnect()
    }

    private fun subscribeStorageItemDef(onNextItem: () -> (Unit)) {

        consumer = ActionCable.createConsumer(URI.create(wsURL))
        val channel = Channel("GraphqlChannel")
        val subscription = consumer?.subscriptions?.create(channel)


        subscription?.onConnected {
            Log.i("ACTIONCABLE", "connected")
        }
                ?.onReceived { _ ->
                    onNextItem() }

        consumer?.connect()
    }
}
