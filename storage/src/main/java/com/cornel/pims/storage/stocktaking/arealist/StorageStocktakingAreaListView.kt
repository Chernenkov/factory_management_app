package com.cornel.pims.storage.stocktaking.arealist

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.cornel.pims.core.barcode.BarcodeHelper
import com.cornel.pims.storage.R
import com.cornel.pims.storage.common.AreaListUnit
import com.cornel.pims.storage.core.app.app
import com.cornel.pims.storage.core.proto.BaseView
import com.cornel.pims.storage.stocktaking.productlist.StorageStocktakingProductListActivity
import kotlinx.android.synthetic.main.common_area_list_item.view.*
import kotlinx.android.synthetic.main.common_list_with_go_back_button.*
import org.jetbrains.anko.intentFor
import javax.inject.Inject

/**
 * Created by KILLdon on 21.02.2018.
 */

interface StorageStocktakingAreaListView {
    val component: StorageStocktakingAreaListComponent
    fun showItems(data: List<AreaListUnit>)
    fun navigateToProductView(zoneId: String)
    fun hidePreloader()
    fun showError(e: Throwable)
    fun finish()
}

class StorageStocktakingAreaListActivity : BaseView(), StorageStocktakingAreaListView {

    @Inject
    override lateinit var presenter: StorageStocktakingAreaListPresenter

    @Inject
    override lateinit var barcodeHelper: BarcodeHelper

    override val component by lazy { app.component.plus(StorageStocktakingAreaListModule(this)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.common_list_with_go_back_button)
        setSupportActionBar(toolbar)

        component.inject(this)

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        presenter.onCreate()

        addListeners()
    }

    fun onRecyclerItemClick(item: AreaListUnit) {
        presenter.onItemSelected(item)
    }

    override fun showItems(data: List<AreaListUnit>) {
        recycler.swapAdapter(
                RecyclerAdapter(data, this::onRecyclerItemClick),
                false)
    }

    private fun addListeners() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            presenter.onRefresh()
        }
        goBackBtn.setOnClickListener {
            presenter.onBackButton()
        }
    }

    override fun navigateToProductView(zoneId: String) {
        startActivity(intentFor<StorageStocktakingProductListActivity>(
                StorageStocktakingProductListActivity.EXTRA_ZONE_ID to zoneId))
    }

    class RecyclerAdapter(private val data: List<AreaListUnit>,
                          private val listener: (AreaListUnit) -> Unit)
        : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {


        // The class is inner because it need access to parent' listener field
        inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
            var view = v
            val itemName: TextView = v.itemName
            val itemCount: TextView = v.itemCount

            fun bind(item: AreaListUnit, listener: (AreaListUnit) -> Unit) {
                itemName.text = item.name
                itemCount.text = item.quantity.toString()
                view.setOnClickListener { listener(item) }
            }
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = data[position]
            holder.bind(item, listener)
        }

        override fun getItemCount() = data.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.common_area_list_item, parent, false)
            return ViewHolder(v)
        }
    }
}