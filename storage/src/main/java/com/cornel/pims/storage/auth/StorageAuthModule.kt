package com.cornel.pims.storage.auth

import dagger.Module
import dagger.Provides

/**
 * Created by AlexGator on 28.02.18.
 */

@Module
class StorageAuthModule(private val activity: StorageAuthView) {

    @StorageAuthScope
    @Provides
    fun providePresenter(): StorageAuthPresenter = StorageAuthPresenterImpl(activity)

    @StorageAuthScope
    @Provides
    fun provideActivity(): StorageAuthView = activity
}
